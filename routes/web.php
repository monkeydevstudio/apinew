<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'v1', 'namespace'=>'v1'],function(){
	Route::get('/', function () {
    	return view('welcome');
	});
	Route::group(['prefix'=>'steam'],function(){
		Route::get('/', 'SteamGoodsController@index');
		Route::get('/{slug}', 'SteamGoodsController@post');
	});
	Route::group(['prefix'=>'gameshop'],function(){
		Route::get('/', 'GamesController@index');
		Route::get('/search', 'GamesController@index');
		Route::get('/all', 'GamesController@index');
		Route::post('/file', 'GamesController@file');
		Route::get('/{slug}', 'GamesController@post');
	});
	Route::group(['prefix'=>'channel'],function(){
		Route::get('/watch/{slug}', 'VideoController@post');
		Route::post('/video', 'VideoController@edit');
		Route::get('/video', 'VideoController@index');
		Route::get('/playlist', 'PlaylistController@index');
		Route::get('/playlist/{slug}', 'PlaylistController@post');	
		Route::get('/urlgenerate', 'ChannelController@urlgenerate');	
	});
	Route::get('/apimap', function(){
	    return view('apimap');
	});
	Route::group(['prefix'=>'components'],function(){
		Route::get('/randomright', 'componentsController@randomRight');
	});
	Route::group(['prefix'=>'projects'],function(){
		Route::get('/', 'projectsController@index');
		Route::get('/{slug}', 'projectsController@post');
	});
	Route::group(['prefix'=>'news'],function(){

		Route::get('/', 'NewsController@index');
		
		//Route::get('/robots', 'newsController@robots');
		//Route::get('/all', 'NewsController@index');
		Route::get('/{slug}', 'NewsController@post');

	});
	Route::group(['prefix'=>'robots'],function(){
		Route::get('/', 'RobotsController@index');
		//Route::get('/robots', 'newsController@robots');
		Route::get('/all', 'RobotsController@index');
		Route::get('/{slug}', 'RobotsController@post');

	});

	Route::post('user/register', 'APIRegisterController@register');
	Route::post('user/login', 'APILoginController@login');
	Route::middleware('jwt.auth')->get('users', function(Request $request) {
    	return auth()->user();
	});
});
