<?php
	namespace App\Behaviours;
	use Illuminate\Http\Request;
	use Carbon\Carbon;
	use DB;

	trait RestfulBehaviour{
		protected $model;
		protected $select;
		protected $modelAttributes = [];
		protected $order = [];
		protected $simillar = [];

		public function index(Request $request){
			$query=$request->toArray();   
	        unset($query['page'],$query['project_id']);   
	        foreach ($query as $k => $v) {
	            if (preg_match('#(max|min)_(.*)#', $k, $o)) {
	                if($o[1]=='min'){
	                    $search['price']['min']=$v;
	                }
	                if($o[1]=='max'){
	                    $search['price']['max']=$v;
	                }
	            }
	            if($k=='title'){
	                $search['title']=mb_strtolower($v); 
				}
				if($k=='genre'){
					$search['genre']=ucfirst($v);
				}
				if($k=='without_dlc'){
					$search['without_dlc']=1; 
				}
				if($k=='activation'){
					$search['activation']=mb_strtolower($v); 
				} 
				if($k=='platform'){

					$search['platform']=mb_strtolower($v); 
				}
				if($k=='active'){
					$search['active']=1; 
				}
				if($k=='without_dlc'){
					$search['without_dlc']=1; 
				}
	            if($k=='sale'){
	                $search['sale']=1; 
	            }
				if($k=='limit'){
	                $search['limit']=$v; 
	            }
	        }

			$model='\\App\\Models\\'.$this->model['main'];
			$projectId=$request->project_id?$request->project_id:1;
			$user_id=$request->user_id;
			$row = $model::select($this->select);
			if(isset($this->model['joined'])){
				foreach ($this->model['joined'] as $k=>$v) {
					if(stristr($v,'projects_users')){		
						/*$row->whereHas($v, function ($query) use (&$projectId){
							$query->where('project_id',$projectId);
						});*/
						$row->join('projects_users','projects.id','=','projects_users.project_id')->where('projects_users.user_id',$user_id);
					}elseif(stristr($v, 'sales')){
						$row->whereRaw('percent > 0')->join('sales', 'sales.goods_id', '=', 'steamgoods.steam_id');
					}elseif(stristr($v,'projects')){
						$row->join($v,strtolower($this->model['main']).'.id','=',$v.'.'.strtolower($this->model['main']).'_id')->where($v.'.project_id',$projectId);	
					}else{
						$row->with($v);
					}
				}
			}	
			collect($this->order)->each(function($o) use (&$row){
				$row->orderBy($o['field'],$o['method']);
			});
			if(isset($search['price'])){
            	$row->whereBetween('price',[$search['price']['min'],$search['price']['max']]);
	        }
	        if(isset($search['title'])){
	            $row->whereRaw("LOWER(title) like ('%$search[title]%')");
			} 
			if ($request->is('v1/steam')) {
				if(isset($search['platform'])){
					$row->whereRaw("JSON_EXTRACT(params, '$.platforms.$search[platform]') = true");
				}
				if(isset($search['genre'])){
					$row->whereRaw("JSON_SEARCH(params, 'one', '$search[genre]') is not Null");
				} 

				$row->whereRaw('sales.status = 0');
			}else{
				if(isset($search['genre'])){
					$row->whereRaw("LOWER(genre) like ('%$search[genre]%')");
				}  
				if(isset($search['activation'])){
					$row->whereRaw("LOWER(activation) like ('%$search[activation]%')");
				} 
				if(isset($search['platform'])){
					$row->whereRaw("LOWER(platform) like ('%$search[platform]%')");
				}
				if(isset($search['without_dlc'])){
					$row->where('dlc', '=', 0);
				}
			}	
	        if(isset($search['sale'])){
				$row->where('sale', '=', 1)->whereRaw('percent > 0');
				
	        }
	        
	        if(isset($search['active'])){
	            $row->where('act', '=', 1);
			}
	
	        if(isset($request->search) && !empty($request->search)){
	            $search = mb_strtolower($request->search); 
	            $row->whereRaw('LOWER(params->\'$."preview"\') like \'%'.$search.'%\' or LOWER(params->\'$."text"\') like \'%'.$search.'%\' or LOWER(title) like \'%'.$search.'%\'');
			} 
			 

	        if($this->model['main'] == 'News'){
	        	$row->whereRaw('news.created_at <= NOW()'); 
			}

			if(!isset($search['limit'])){
				$row=$row->Paginate(21); 
			}else{
				$row = $row->Paginate(ceil($search['limit']));
			}
			$row->appends($query);
			//return  response()->json($row)->header('Cache-Control','max-age=3600, public');
			return  response()->json($row);
		}

		public function post(Request $request, $slug){

			$model='\\App\\Models\\'.$this->model['main'];
			if($this->model['main']!=='Projects'){
				$row=$model::where('slug','=',$slug)->orWhere('id',$slug);
			}else{
				$row=$model::where('id',$slug);
			}
			foreach ($this->model['joined'] as $k=>$v) {
				if(!stristr($v,'projects') && !stristr($v,'sales')){		
					$row->with($v);
				}
			}
			$row=$row->first();
			if(is_array($this->simillar) && !empty($this->simillar) && !empty($row)){
				$rows=$row->toArray();	
				$simillar=$model::select($this->select);
			
				if(stristr($row[$this->simillar['field']],',')){
					$simField=explode(',', $row[$this->simillar['field']]);
					;
					shuffle($simField);
					$simillar->where($this->simillar['field'],'like',"%$simField[0]%")->limit($this->simillar['limit']);
				}else{
					$simField=$row[$this->simillar['field']];
					$simillar->where($this->simillar['field'],'like','%'.$simField.'%')->limit($this->simillar['limit']);
				}
				$simillar->inRandomOrder();
				$rows['similar']=$simillar->get();
				$row=collect($rows);
			}			
			return  response()->json($row)->header('Cache-Control','max-age=3600, public');
		}

		public function create(){
			echo 3;
		}

		public function edit(){
			return 4;
		}

		public function delete(){
			echo 5;
		}

		public function file(Request $request){
			return $request->method();
		}
	}
?>