<?php

namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChannelController extends Controller
{
    use RestfulBehaviour;
}
