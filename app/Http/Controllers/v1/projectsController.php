<?php

namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Projects;

class projectsController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){  
    
      $this->model=['main'=>'Projects','joined'=>['projects_users']];
      $this->select=['projects.*'];
      $this->order=[['field'=>'created_at','method'=>'desc']];
    }
}
