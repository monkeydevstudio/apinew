<?php
namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News; 

class NewsController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){	
  		$this->model=['main'=>'News','joined'=>['projects_news']];
  		$this->select=['news.*'];
  		$this->order=[['field'=>'news.created_at','method'=>'desc']];
  	}
}

