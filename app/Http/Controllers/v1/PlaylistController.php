<?php
namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Playlists; 

class PlaylistController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){  
    
      $this->model=['main'=>'Playlists','joined'=>['projects_playlists','videos']];
      $this->select=['*'];
      $this->order=[['field'=>'playlists.created_at','method'=>'desc']];
    }
}