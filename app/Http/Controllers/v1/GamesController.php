<?php
namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Games; 

class GamesController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){	
  	
  		$this->model=['main'=>'Games','joined'=>['projects_games']];
  		$this->select=['games.id','games.params','games.price','games.act','games.status','games.percent'];
  		$this->order=[['field'=>'available','method'=>'desc'],['field'=>'percent','method'=>'desc'],['field'=>'price','method'=>'desc']];
  		$this->simillar=['field'=>'genre','limit'=>4];
  	}
}
