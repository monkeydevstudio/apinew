<?php
namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Videos; 

class VideoController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){	
  	
  		$this->model=['main'=>'Videos','joined'=>['projects_videos','playlist']];
  		$this->select=['videos.*'];
  		$this->order=[['field'=>'videos.created_at','method'=>'desc']];
  		$this->simillar=['field'=>'playlist_id','limit'=>15];
  	}
}