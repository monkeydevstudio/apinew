<?php

namespace App\Http\Controllers\v1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Videos;
use App\Models\Playlists;
use App\Models\Games;
use App\Models\News;

class componentsController extends Controller{
    public function randomRight(){
        $rand['playlist']=Playlists::inRandomOrder()->first();    
        $rand['video']=Videos::inRandomOrder()->first();    
        $rand['games']=Games::inRandomOrder()->first();    
        $rand['news']=News::inRandomOrder()->first();    
        //return response()->json($rand)->header('Cache-Control','max-age=3600, public');        
        return $rand;        
    }
}