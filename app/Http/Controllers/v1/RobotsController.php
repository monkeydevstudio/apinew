<?php
namespace App\Http\Controllers\v1;

use App\Behaviours\RestfulBehaviour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RobotsController extends Controller
{
    use RestfulBehaviour;
    public function __construct(){	
  	
  		$this->model=['main'=>'Robots'];
  		$this->select=['*'];
  		$this->order=[['field'=>'created_at','method'=>'desc']];
  	}
}

