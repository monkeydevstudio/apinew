<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamGoods;

class steamPriceParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steam:priceParse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/?format=jso';

        //{"applist":{"apps":[{"appid":5,"name":"Dedicated Server"}
        $json = json_decode(file_get_contents($url),1);
        $i = 0;
        foreach ($json['applist']['apps'] as $k => $v) {
            steamGoods::firstOrCreate(['steam_id'=> $v['appid'], 'title'=> $v['name']]);
            echo "$v[name] success inserted in table \n";
        }
    }
}
