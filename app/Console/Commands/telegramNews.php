<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Publishs;
use App\Models\Telegrams;
use App\Models\Robots;

class telegramNews extends Command 
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:watch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic parser news feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        for($i=0; $i<11; $i++){
            $updates = json_decode(file_get_contents('php://input') ,true);
            
            $s = 1; 

            //$json = json_decode(file_get_contents('https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/getUpdates'),1);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/getUpdates');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_PROXY, '181.215.129.249:65233');
            curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
            curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
            curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
            $json = json_decode(curl_exec($curl),1);
            curl_close($curl);

            foreach ($json['result'] as $k => $j) {
                if(isset($j['callback_query'])){
                    
                    $robot_id = str_replace('/publish_', '', $j['callback_query']['data']);
                    $tid = Publishs::firstOrCreate([
                        'robot_id'=>$robot_id
                    ],[
                        'robot_id'=>$robot_id,
                        'status'=>0 
                    ]);
                    


                    if($tid->status === 0){
                        $robots = Robots::where('id', $robot_id)->first();
                        $text=$robots->params['preview']."\n\n".$robots->params['description'];
                        
                        $text=mb_substr($text, 0, 2000);

                        $text=urlencode($text."\n\n В очереди на публикацию.");
                       

                        $message_id = Telegrams::where('robot_id', $robot_id)->first();

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, "https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/editMessageText?chat_id=@ETNewsParser&text=$text&message_id=$message_id->tid");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                        curl_setopt($curl, CURLOPT_PROXY, '181.215.129.249:65233');
                        curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
                        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
                        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
                        $upd = json_decode(curl_exec($curl),1);
                        curl_close($curl);

                        Publishs::where('robot_id', $robot_id)->update(['status'=>1]);
                    }
                    //editMessageText
                    //print_r($message_id);
                }
            }
            sleep(5);
        }
    }
}
