<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class vkbot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:vk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'VK api worker';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = true;
        while ($i != false) {
            echo $i++;
        }
    }
}
