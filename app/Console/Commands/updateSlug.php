<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamGoods;

class updateSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slug:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json=steamGoods::select('id','title')->get();
        $count=steamGoods::count();
        foreach ($json as $k=>$j) {
            steamGoods::where('id','=',$j->id)->update(['slug'=>str_slug($j->title)]);
            echo "$j->title slug success update ($k/$count)\n";
        }
       
    }
}
