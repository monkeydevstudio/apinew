<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Publishs;
use App\Models\Robots;
use App\Models\News; 
use App\Models\ProjectsNews;

class newsPublish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'News publisher';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //date_default_timezone_set('Asia/Yekaterinburg');   
        date_default_timezone_set('Europe/Moscow');   
    
        $last = News::orderBy('created_at', 'desc')->first();
        
        if(strtotime('+ 30 minute',strtotime($last->created_at)) < strtotime(date("Y-m-d H:i:s"))){
            $next = date("Y-m-d H:i:s");
        }else{
            $next = date('Y-m-d H:i:s', strtotime('+ 30 minute',strtotime($last->created_at)));
        }
      
        $publish = Publishs::where('publish', 0)->first();
        $robot = Robots::where('id', $publish->robot_id)->first();           

        $newNews = News::create([
            'title'=>$robot->title,
            'slug'=>str_slug($robot->title),
            'act'=>1,
            'created_at'=>$next,
            'updated_at'=>$next,
            'params'=>[
                'title'=>$robot->title,
                'preview'=>$robot->params['preview'],
                'text'=>$robot->params['description'],
                'source'=>$robot->link,
                'author'=>'EpicType Press',
            ]
        ]);
        ProjectsNews::create(['news_id'=>$newNews->id, 'project_id'=>1]);
        Publishs::where('robot_id', $publish->robot_id)->update(['publish'=>1]);
    }
}
