<?php

namespace App\Console\Commands;

use Storage;
use Illuminate\Console\Command;
use App\Models\Games;


class downloadPhoto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:photo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games=Games::select('games.*')->join('projects_games','projects_games.games_id','=','games.id')->where('projects_games.project_id',1)->get();
        //print_r($games->toArray());
        $images=[];
        foreach ($games as $v) {
            if(isset($v->params['attachment']['images'])){
                foreach ($v->params['attachment']['images'] as $k => $i) {
                    //$images[$v->sbid][]=$i;
                    $url = $i;
                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    Storage::put('games/'.$v->sbid.'/g'.$v->sbid.'_'.$k, $contents);
                    echo 'save to folder: '.$v->sbid.' file: '.$v->sbid.'/g'.$v->sbid.'_'.$k."\n";
                }
            }
        }
    }
}
