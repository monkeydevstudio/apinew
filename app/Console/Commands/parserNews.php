<?php

namespace App\Console\Commands;
use voku\helper\HtmlDomParser;
use Illuminate\Console\Command;
//use Sunra\PhpSimple\HtmlDomParser;
use App\Models\Feeds;
use App\Models\Robots;
use App\Models\Rel;
use App\Models\Telegrams;

class parserNews extends Command 
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic parser news feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rel=Rel::where('act',1)->get();
        $fields=collect($rel)->pluck(NULL,'id');
        foreach($rel as $v){
            $rel_id[]=$v->id;
        }

        //$feeds=Feeds::whereIn('rel_id',$rel_id)->where('act',1)->get();
        $feeds=Feeds::where('rel_id',1)->where('act',1)->get();
        foreach ($feeds as $k => $v) {
            echo $v->rel_id."\n";    
            $rss=simplexml_load_string(file_get_contents($v->url));  
            foreach ($rss->channel->item as $r) {   
                echo (string)$r->title."\n";
                $link=(string)$r->link;   

                $descArr=HtmlDomParser::file_get_html($link)->find($fields[$v->rel_id]['text']);
                if(isset($descArr[0])){
                    $description = HtmlDomParser::file_get_html($link)->find($fields[$v->rel_id]['text'])[0]->plaintext;
                
                    $description=str_replace(array('!!!!SIMPLE_HTML_DOM__VOKU__AMP!!!!','!!!!SIMPLE_HTML_DOM__VOKU__PERCENT!!!!','!!!!SIMPLE_HTML_DOM__VOKU__PLUS!!!!'),array('&','%','+'),$description);
                    //echo $description."\n\n"; 
                    
                    $text = urlencode("$r->description\n\n$description");
                    
                    $robot=array(
                        'title'=>(string)$r->title,
                        'preview'=>trim((string)$r->description),
                        'description'=>trim($description),
                        'link'=>(string)$r->link,
                        'image'=>$r->enclosure,
                        'publicate'=>(string)$r->pubDate,
                        'category'=>$v->category,
                        'rel_id'=>$v->rel_id,
                    );
                   
                    $post = Robots::firstOrCreate([
                        'link'=>$robot['link'],
                    ],[
                        'title'=>$robot['title'],
                        'created_at'=>date('Y-m-d H:i:s',strtotime($robot['publicate'])),
                        'link'=>$robot['link'],
                        'params'=>$robot,
                        'category'=>$robot['category'],
                        'rel_id'=>$robot['rel_id'],
                        'act'=>1,
                    ]); 

                    
                    $inline_button = array("text"=>"Опубликовать","callback_data"=>"/publish_$post->id");
                    $inline_keyboard = [[$inline_button]];
                    $keyboard=array("inline_keyboard"=>$inline_keyboard);
                    $replyMarkup = json_encode($keyboard);

                    $tid = Telegrams::firstOrCreate([
                        'robot_id'=>$post->id
                    ],[
                        'robot_id'=>$post->id,
                        'status'=>0 
                    ]);

                    if($tid->status !== 1 ){
                        $text = urlencode(mb_substr($description, 0, 2000));
                        
                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, "https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/sendMessage?chat_id=@ETNewsParser&text=$text&reply_markup=$replyMarkup");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                        curl_setopt($curl, CURLOPT_PROXY, '191.96.57.38:65233');
                        curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
                        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
                        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
                        $callback = json_decode(curl_exec($curl),1);
                        curl_close($curl);
                        if(isset($callback['result'])){
                            /*$callback = json_decode(file_get_contents("https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/sendMessage?chat_id=@ETNewsParser&text=$text&reply_markup=$replyMarkup"),1); */
                            Telegrams::where('robot_id', $post->id)->update(['status'=>1, 'tid'=>$callback['result']['message_id']]);     
                        }
                    }
                }
                sleep(5);
            }
        }
    }
}
