<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;

class etnewspars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:etnews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser epictype.ru old (news)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json=json_decode(file_get_contents('http://epictype.ru/cron/newsexport.php'));
        //print_r($json);   
        foreach($json as $k => $v){   
            News::updateOrCreate(
                [
                'pid'=>$v->id,   
            ],[
                'title'=>$v->title,
                'slug'=>str_slug($v->title),
                'created_at'=>$v->t_add,
                'params'=>$v,
                'act'=>1,
                'pid'=>$v->id,
            ]);
            echo 'Create|update '.$v->title."\n";
        }
    }
}
