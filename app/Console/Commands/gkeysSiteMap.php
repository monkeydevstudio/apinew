<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Games;
class gkeysSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gkeys:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games=Games::select('params')->join('projects_games','games.id','projects_games.games_id')->where('projects_games.project_id',3)->get();
        $data=
            '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n".
            '<url>'."\n".'<loc>http://gkeys.su</loc>'."\n".'</url>'."\n";
        $i=1;    
        foreach ($games as $k => $g) {
            if(isset($g->params['slug'])){
                $data.='<url>'."\n".'<loc>http://gkeys.su/'.$g->params['slug'].'</loc>'."\n".'  </url>'."\n";
                $i++;
            }
        } 
        $data.='</urlset>';
        echo "generated $i urls\n";
        file_put_contents('sitemap.xml', $data, FILE_APPEND);
    }
}
