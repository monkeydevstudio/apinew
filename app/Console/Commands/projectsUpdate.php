<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News;
use App\Games;
use App\Projects;
use App\ProjectsNews;
use App\ProjectsGames;
use App\ProjectsChannels;

class projectsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'project update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $news=News::select('id','title')->get()->toArray();
        $games=Games::select('id','title')->get()->toArray();
        $gamesGkeys=Games::select('id','title')->where('percent','>=',5)->get()->toArray();
        foreach ($news as $k => $v) {   
            ProjectsNews::updateOrCreate(
                [
                   'news_id'=>$v['id'],
                   'project_id'=>1,     
                ],
                [
                    'project_id'=>1,
                    'news_id'=>$v['id'],
                ]
            );
            echo 'Create|update news '.$v['title']."\n";
        }
        foreach ($games as $k => $v) {   
            ProjectsGames::updateOrCreate(
                [
                   'games_id'=>$v['id'],
                   'project_id'=>1,     
                ],
                [
                    'project_id'=>1,
                    'games_id'=>$v['id'],
                ]
            );
            echo 'Create|update games '.$v['title']."\n";
        }
        foreach ($gamesGkeys as $k => $v) {   
            ProjectsGames::updateOrCreate(
                [
                   'games_id'=>$v['id'], 
                   'project_id'=>3,    
                ],
                [
                    'project_id'=>3,
                    'games_id'=>$v['id'],
                ]
            );
            echo 'Create|update games for gkeys.su '.$v['title']."\n";
        }
    }
}
