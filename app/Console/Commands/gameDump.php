<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamgoods;
use Storage;

class gameDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steam:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        
        Storage::deleteDirectory('steam');
        Storage::makeDirectory('steam');
        $countRow=steamGoods::count();
        $count = ceil($countRow/10000);

        for($i=0; $i<$count; $i++){
            echo "====== start step $i ======\n\n";
            $json = steamGoods::limit(10000)->offset($i*10000)->get();
            $jsonArr = [];
            
            foreach ($json as $j) {
                $jsonArr[]=array(
                    'steam_id' => $j->steam_id,
                    'title' => $j->title,
                    'price' => $j->price,
                    'params' => $j->params,
                    'slug' => $j->slug,
                    'status' => $j->status,
                    'parent_id' => $j->parent_id,
                    'created_at' => $j->created_at
                );
                echo $j->title." success dumped\n";
            }

            $output=json_encode($jsonArr);
            Storage::disk('local')->put("steam/steam$i.json", $output);
            echo "\n====== end step $i ======\n\n";
        }
    }
}
