<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\News;
use App\Models\Videos;
use App\Models\Playlists;
use App\Models\Games;
use App\Models\steamgoods;

class etsitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'et:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        // print_r($news);
        $data='';
        file_put_contents('sitemap.xml', $data);    
        $news=News::select('pid','slug')->get();
        $video=Videos::select('url','slug')->get();
        $playlist=Playlists::select('url','slug')->get();
        $games=Games::get();
        $sales=steamgoods::select('slug')->get();

        /*
            <?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
            <url>
                <loc>http://site-on.net/</loc>
                <lastmod>2013-06-04</lastmod>
                <changefreq>weekly</changefreq>
                <priority>1.0</priority>
            </url>
            <url>
                <loc>http://site-on.net/about</loc>
                <lastmod>2013-06-04T08:34:48+01:00</lastmod>
                <priority>1.0</priority>
            </url>
            <url>
                <loc>http://site-on.net/create</loc>
                <priority>1.0</priority>
            </url> 
            </urlset>
        */

   
        $data=
            '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n".
            '<url>'."\n".'<loc>http://epictype.ru</loc>'."\n".'</url>'."\n".
            '<url>'."\n".'<loc>http://epictype.ru/video</loc>'."\n".'</url>'."\n".
            '<url>'."\n".'<loc>http://epictype.ru/video/playlist</loc>'."\n".'</url>'."\n".
            '<url>'."\n".'<loc>http://epictype.ru/news</loc>'."\n".'</url>'."\n".
            '<url>'."\n".'<loc>http://epictype.ru/shop</loc>'."\n".'</url>'."\n";
            '<url>'."\n".'<loc>http://epictype.ru/sales</loc>'."\n".'</url>'."\n";
        $i=6;
        foreach ($video as $k => $v) {
            $data.='<url>'."\n".'<loc>http://epictype.ru/video/watch/'.$v->slug.'</loc>'."\n".'</url>'."\n";
            $i++;
        } 
        foreach ($playlist as $k => $p) {
            $data.='<url>'."\n".'<loc>http://epictype.ru/video/playlist/'.$p->slug.'</loc>'."\n".'</url>'."\n";
            $i++;
        }
        foreach ($news as $k => $n) {
            $data.='<url>'."\n".'<loc>http://epictype.ru/news/'.$n->slug.'</loc>'."\n".'</url>'."\n";
            $i++;
        }       

        foreach ($games as $k => $g) {
            $data.='<url>'."\n".'<loc>http://epictype.ru/shop/'.$g->slug.'</loc>'."\n".'</url>'."\n";
            $i++;
        } 
        /*foreach ($sales as $k => $s) {
            $data.='<url>'."\n".'<loc>http://epictype.ru/sales/'.$s->slug.'</loc>'."\n".'</url>'."\n";
            $i++;
        }*/ 
       
        $data.='</urlset>'; 
        echo "generated $i urls\n";
        file_put_contents('public/epictype/sitemap.xml', $data, FILE_APPEND);

    }
}
