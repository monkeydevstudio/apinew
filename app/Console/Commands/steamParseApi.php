<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamgoods;

class steamParseApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steam:games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        /*$url = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/?format=json';

        //{"applist":{"apps":[{"appid":5,"name":"Dedicated Server"}
        $json = json_decode(file_get_contents($url),1);

        foreach ($json['applist']['apps'] as $k => $v) {
            steamGoods::Create(['steam_id'=> $v['appid'], 'title'=> $v['name']]);
            echo "$v[name] success inserted in table \n";
        }

        /*http://store.steampowered.com/api/appdetails?appids= товар*/
        $row = steamGoods::where('sales.status',0)->where('steamgoods.status', 0)->join('sales', 'sales.goods_id', '=', 'steamgoods.steam_id')->limit(12)->get(); 
        foreach ($row as $k => $v) {
            $json = json_decode(file_get_contents('http://store.steampowered.com/api/appdetails?l=ru&appids='.$v['steam_id']),1);
            $json = $json[$v['steam_id']];
            if(isset($json['data'])){
               steamGoods::where('steam_id',$v['steam_id'])->update(['params'=>json_encode($json['data']), 'status'=>1]);
               echo "$v[title] ($v[steam_id]) => success update \n";
            }else{
                echo "$v[title] ($v[steam_id]) => no data\n";
                steamGoods::where('steam_id',$v['steam_id'])->update(['status'=>2]);
            }
            
            sleep(5);
        }
        //print_r($row->toSql());
    }
}
