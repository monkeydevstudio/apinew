<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Games;
use App\Models\vkgoods;
use Storage;

class vkStorePublicate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vkshop:publicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //https://api.vk.com/method/messages.getDialogs?count=20&version=5.74&access_token=52e5be57e7c6c1b0e2e54bf796b8361029a6c606d6cd89555a43a462e76bdfff10331fe0cdbb740639e23
        $token='a5b684b16d34ed9f066cf5603492b0bbdbb8279860e4441dfa798a3c47b8b49a2d8fefad48077af50e0ba';
        $group_id=75046591;
        $games=Games::select('games.*')
            ->join('projects_games','projects_games.games_id','=','games.id')
            ->leftJoin('vkgoods','vkgoods.games_id','=','games.id')
            ->where('projects_games.project_id',3)
            ->where('vkgoods.games_id')
            ->get();

        $i=0;    
        foreach ($games as $v) {
            //print_r($v);
            $files=Storage::allFiles('/games/'.$v->sbid);
            if(!empty($files)){
                $upload_url=json_decode(file_get_contents("https://api.vk.com/method/photos.getMarketUploadServer?access_token=$token&group_id=$group_id&main_photo=1&version=5.74"));
                $img='/home/demon/www/api/storage/app/'.$files[0];
                $post_params = array(
                    "file$i" => new \CURLFile($img),
                    'main_photo'=>1,
                );
                $ch = curl_init( $upload_url->response->upload_url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
                $response = curl_exec( $ch );
                curl_close( $ch );
                $response=json_decode($response);
                if(isset($response->photo)){ 
                    $urlParams = array(
                        'access_token'=>$token,
                        'group_id'=>$group_id,
                        'photo'=>$response->photo,
                        'server'=>$response->server,
                        'hash'=>$response->hash,
                        'version'=>'5.74',
                    );
                    $urlParams=implode('&',$urlParams);
                    $saveFile=json_decode(file_get_contents("https://api.vk.com/method/photos.saveMarketPhoto?access_token=$token&group_id=$group_id&photo=$response->photo&server=$response->server&hash=$response->hash&crop_data=$response->crop_data&crop_hash=$response->crop_hash&version=5.74"),1);
                    //print_r($saveFile); 
                    $goods=array(
                        'owner_id' => '-75046591',
                        'name' => $v->title,
                        'description' => $v->params['description'],
                        'price'=>($v->price==0?0.01:$v->price),
                        'category_id'=>804,
                        'deleted'=>($v->price==0?1:0),
                        'main_photo_id'=>$saveFile['response'][0]['pid'],
                        'access_token'=>$token,
                        'version'=>'5.74',
                    );

                    $ch = curl_init( "https://api.vk.com/method/market.add");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
                    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $goods);
                    $saveGoods = curl_exec( $ch );
                    curl_close( $ch );
                    $saveGoods=json_decode($saveGoods,1);
                    if(isset($saveGoods['response']['market_item_id'])){
                        vkgoods::Create([
                            'games_id'=>$v->id,
                            'goods_id'=>$saveGoods['response']['market_item_id'],
                            'groups_id'=>$group_id,
                        ]);
                        echo 'Товар ('.$v->title.') успешно загружен'."\n";
                    }
                }else{
                    echo "photo err id:$v->sbid ($response->error)\n";
                }
            }else{
                echo "No photo ($v->title) \n";
            }
            //market_item_id
            
            //print_r($saveGoods);
        }
    }
}
