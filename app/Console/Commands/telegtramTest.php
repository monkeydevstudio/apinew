<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class telegtramTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/getUpdates');
        curl_setopt($ch, CURLOPT_PROXY, '191.96.57.38:65233');
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
        curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $out = curl_exec($ch);
        echo $out;*/

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/getUpdates');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_PROXY, '181.215.129.249:65233');
        curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
        $out = curl_exec($curl);
        print_r(json_decode($out,1));
        curl_close($curl);
    }
}
