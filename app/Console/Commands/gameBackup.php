<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamgoods;
use App\Models\ProjectsSteam;
use Storage;

class gameBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steam:backup {s}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $s = $this->argument('s');
        if(!isset($s)){
            $s=0;
        }   
     
 
        for($i=$s; $i<7; $i++){
            $file = Storage::get("steam/steam$i.json");
            $json = json_decode($file,1);
           
            foreach ($json as $k=>$j) {
                
                /*print_r($j);*/
                $arr = array(
                    'sale' => 0,
                    'price' => 0,
                    'percent' => 0,
                    'dlc' => 0,
                    'parent_id' => 0,
                    'act' => 0,
                );

                if(!is_null($j['params'])){
                    if(is_array($j['params'])){
                        if(array_key_exists('price_overview', $j['params'])){
                            $arr['sale'] = ($j['params']['price_overview']['discount_percent']>0 ? 1 : 0);
                            $arr['percent'] = ($j['params']['price_overview']['discount_percent']);
                            $arr['price'] = $j['params']['price_overview']['final']/100;
                        }
                        if(array_key_exists('type', $j['params']) && $j['params']['type'] === 'dlc'){
                            $arr['dlc'] = 1;
                            $arr['parent_id'] = $j['params']['fullgame']['appid'];
                        }
                    }else{
                        $arr['sale'] = 0;
                        $arr['percent'] = 0;
                        $arr['price'] = 0;
                        $arr['dlc'] = 0;
                        $arr['parent_id'] = 0;
                        $j['params'] = 'null';
                    }
                    
                }
                
                if($j['params'] !== null){
                    $row=steamGoods::firstOrCreate([
                            'steam_id'=> $j['steam_id']
                        ],
                        [
                            'steam_id' => $j['steam_id'],
                            'title' => $j['title'],
                            'price' => $arr['price'],
                            'params' => $j['params'],
                            'slug' => $j['slug'],
                            'status' => $j['status'],
                            'parent_id' => $j['parent_id'],
                            'created_at' => (is_array($j['created_at']) ? str_replace('.000000', '', $j['created_at']['date']) : $j['created_at'] ),
                            'sale' => $arr['sale'],
                            'percent' => $arr['percent'],
                            'parent_id' => ($arr['parent_id'] !== null ? $arr['parent_id'] : 0),
                            'dlc' => $arr['dlc'],
                            'act' => ($arr['sale'] === 1 ? 1 : 0),
                        ]
                    );

                    ProjectsSteam::firstOrCreate([
                            'steamgoods_id'=>$row->id,
                            'project_id'=>1,
                        ],
                        [
                            'steamgoods_id'=>$row->id,
                            'project_id'=>1,
                        ]
                    );
                    ProjectsSteam::firstOrCreate([
                            'steamgoods_id'=>$row->id,
                            'project_id'=>2,
                        ],
                        [
                            'steamgoods_id'=>$row->id,
                            'project_id'=>2,
                        ]
                    );     
                    print_r($arr);
                }
                $index = ($i===0? $k : $k+($i*10000));
                echo "$j[title] success backup ($index/70000)\n";
            }  
        }     
    }
}
