<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Videos;
use App\Models\Playlists;
use App\Models\ProjectsPlaylists;
use App\Models\ProjectsVideos;
use DB;

class channelParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:channel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser youtube channels playlist & video';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $playlistTable=new Playlists; 
        $playlistTable->setConnection('channel');    
        $VideoTable=new Videos;
        $VideoTable->setConnection('channel');  
        $key='AIzaSyDIEdit83j8NiOZiWgBRI8HNTJblqcDRrE';   
        $token='ya29.GlsFBV8aUblJcvXPAmtg0hXj5uVLXF0at90i1Hq-kIUswuNDgpzTD_T5nsNyuhNrHgBsjPFw3IUd4WWTmEY8CTsaCOg5rf3ef5yLoJqUyn3vF8OONVlvTeXBOhwr';   
        
        $pager=array(
            1=>'CDIQAQ',
            2=>'CDIQAA',
            3=>'CGQQAA',
            4=>'CJYBEAA'
        );
        

        echo 'Update|Create Playlist'."\n\n";
        $json=json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/playlists?part=contentDetails,snippet&channelId=UCBs_DU__209324ZEufa2Uqw&key='.$key),1);
        $count=$json['pageInfo']['totalResults'];

        if($count>50){
            for($i=1; $i<ceil($count/50)+1; $i++){
                $json=json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/playlists?part=contentDetails,snippet&channelId=UCBs_DU__209324ZEufa2Uqw&pageToken='.$pager[$i].'&maxResults=50&key='.$key),1);
                foreach ($json['items'] as $kk => $vv) {
                    $playlistTable->updateOrCreate(
                        [
                            'url'=>$vv['id']
                        ],
                        [
                            'params'=>$vv,
                            'created_at'=>$vv['snippet']['publishedAt'],
                            'channel_id'=>$vv['snippet']['channelId'],
                            'act'=>1,
                            'slug'=>str_slug($vv['snippet']['title']),
                            'title'=>$vv['snippet']['title'],
                            'updated_at'=>date('Y-m-d H:i:s')   
                        ]
                    );
                    echo 'updated: '.$vv['id']."\n";
                }
            }
        }
        echo "\n";
        echo "Update|Create videos"."\n\n";
        
        $playlists=$playlistTable->where('url','<>','plempty')->where('act',1)->get();
        //print_r($playlists);
        foreach ($playlists as $k => $v) {
            if($v->params['contentDetails']['itemCount']<51){
                $json=json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails%2Csnippet&playlistId=$v->url&maxResults=50&key=$key"),1);
                foreach ($json['items'] as $kk => $vv) { 
                    /*Video::where('url',$vv['contentDetails']['videoId'])->update([
                        'params'=>$vv,
                        'created_at'=>$vv['snippet']['publishedAt'],
                        'channel_id'=>$vv['snippet']['channelId'],
                        'act'=>1,
                        'playlist_id'=>$vv['snippet']['playlistId'],
                        'slug'=>str_slug($vv['snippet']['title']),
                        'updated_at'=>date('Y-m-d H:i:s')    
                    ]);*/
                    $VideoTable->updateOrCreate(
                        [
                            'url'=>$vv['contentDetails']['videoId']
                        ],
                        [
                            'params'=>$vv,
                            'url'=>$vv['contentDetails']['videoId'],
                            'created_at'=>$vv['snippet']['publishedAt'],
                            'channel_id'=>$vv['snippet']['channelId'],
                            'act'=>1,
                            'playlist_id'=>$vv['snippet']['playlistId'],
                            'title'=>$vv['snippet']['title'],
                            'slug'=>str_slug($vv['snippet']['title']),
                            'updated_at'=>date('Y-m-d H:i:s') 
                        ]
                    );
                    echo 'updated: '.$vv['contentDetails']['videoId']." => ".$vv['snippet']['title']."\n";
                }
            }else{
                $page=ceil($v->params['contentDetails']['itemCount']/50);
                for($i=1; $i<ceil($v->params['contentDetails']['itemCount']/50)+1; $i++){
                    $json=json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails%2Csnippet&playlistId='.$v->url.'&maxResults=50&pageToken='.$pager[$i].'&key='.$key),1);
                    foreach ($json['items'] as $kk => $vv) { 
                        $VideoTable->updateOrCreate(
                            [
                                'url'=>$vv['contentDetails']['videoId']
                            ],
                            [
                                'params'=>$vv,
                                'url'=>$vv['contentDetails']['videoId'],
                                'created_at'=>$vv['snippet']['publishedAt'],
                                'channel_id'=>$vv['snippet']['channelId'],
                                'act'=>1,
                                'playlist_id'=>$vv['snippet']['playlistId'],
                                'title'=>$vv['snippet']['title'],
                                'slug'=>str_slug($vv['snippet']['title']),
                                'updated_at'=>date('Y-m-d H:i:s') 
                            ]
                        );
                        echo 'updated: '.$vv['contentDetails']['videoId']." => ".$vv['snippet']['title']."\n";
                    }    
                }
            } 
        }
        echo "\n";
        $row=Videos::select('id','title')->get();
        //print_r($row);
        $row->each(function($v){
            //echo $v->id."\n"; 
            ProjectsVideos::firstOrCreate(['videos_id'=>$v->id]);
            echo 'updated projects for: '.$v['title']." ($v->id) \n";  
        });

        $row=Playlists::select('id','title')->get();
        //print_r($row);
        $row->each(function($v){
            //echo $v->id."\n"; 
            ProjectsPlaylists::firstOrCreate(['playlists_id'=>$v->id]);
            echo 'updated projects for: '.$v['title']." ($v->id) \n";  
        });
        exit;
        /*$json=json_decode(file_get_contents("http://epictype.ru/cron/youtube.php"),1);
        DB::table('playlists')->insert($json);*/
    }
}
