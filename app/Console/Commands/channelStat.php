<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Video;
use App\Statistics;
use App\StatisticsLog;

class channelStat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:channelStat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser youtube channels statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key='AIzaSyDIEdit83j8NiOZiWgBRI8HNTJblqcDRrE';     
        $token='ya29.GlsFBV8aUblJcvXPAmtg0hXj5uVLXF0at90i1Hq-kIUswuNDgpzTD_T5nsNyuhNrHgBsjPFw3IUd4WWTmEY8CTsaCOg5rf3ef5yLoJqUyn3vF8OONVlvTeXBOhwr';     
        $video=Video::get();
        foreach ($video as $k => $v) {
            echo "start: $v->url\n";    
            $json=json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=$v->url&part=statistics,id&key=$key"),1);
            if(!empty($json['items'])){    
                Statistics::updateOrCreate([
                    'video_id'=>$json['items'][0]['id']
                ],
                [
                    'video_id'=>$json['items'][0]['id'],
                    'params'=>json_encode($json['items'][0]['statistics']),
                    'type'=>'youtube'
                ]);
                StatisticsLog::create([
                    'video_id'=>$json['items'][0]['id'],
                    'params'=>json_encode($json['items'][0]['statistics']),
                    'type'=>'youtube'
                ]);
                echo "updated: $v->url\n\n";
            }else{
                echo "error: $v->url\n\n";
            } 
        } 
    }
}
