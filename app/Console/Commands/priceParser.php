<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class priceParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:plati';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser prices from plati.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        /*$xml=simplexml_load_file('http://www.plati.io/api/yml.ashx');
        //print_r($xml);
        // shop -> categories -> category []
    	//print_r($xml->shop->offers->offer);
        //foreach ($xml->shop->categories->category as $k => $v) {
    	foreach ($xml->shop->offers->offer as $k => $v) {
    		$name=str_replace(array('CD-Key','Gift','(Steam)','(PC)','(XBOX)','(PS)','(Uplay)','(EURO)','(RU)','(US)','(RUS)','(Honor)','(Origin)','(iOs)','(Kalypso Launcher)','(USA)','(EURO/RUS/USA)','(игра, 2014)', 'RU-CIS-UA','+ ПОДАРОК','(Steam Россия)','RU/CIS','( STEAM KEY RU + CIS)','(STEAM GIFT / RU/CIS)','ASIA     / MULTI)','RU+СНГ','[STEAM]','(Region Free)','REGION FREE',' + СКИДКИ'),'',$v->name);
    		DB::table('price_parser')->insert(['name'=>$name]);
    	}*/
 
        $row=DB::table('price_parser')->get();
        
        foreach($row as $k=>$v){  
            echo 'http://www.plati.io/api/search.ashx?query='.urlencode($v->name).'&response=json&pagesize=500'."\n"; 
            $json=json_decode(file_get_contents('http://www.plati.io/api/search.ashx?query='.urlencode($v->name).'&response=json&pagesize=500'),1);
            foreach ($json['items'] as $kk => $vv) {
                $prices[ceil($vv['price_rur'])]=array('price'=>$vv['price_rur'],'id'=>$vv['id'],'url'=>$vv['url']);
                
            }
            //print_r($prices);
            if(!empty($prices)){
                $val = array_column($prices, 'price');
                $min=ceil(min($val));
                $max=ceil(max($val));
                echo 'min: '.$min.' max: '.$max."\n";
                DB::table('price_parser')->where('id',$v->id)->update([
                    'min'=>$min,
                    'max'=>$max,
                    'id_min'=>$prices[$min]['id'],
                    'id_max'=>$prices[$max]['id'],
                    'url_min'=>$prices[$min]['url'],
                    'url_max'=>$prices[$max]['url'],
                ]); 
                DB::table('price_log')->insert([
                    'name'=>$v->name,
                    'min'=>$v->min,
                    'max'=>$v->max,
                    'created'=>$v->created,
                ]);

            }
            $prices=array();           
        }
       /* foreach($row as $k=>$v){
            
        }*/   
        
    }
}
