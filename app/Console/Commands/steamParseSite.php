<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Sunra\PhpSimple\HtmlDomParser;
use App\Models\Sales;
use App\Models\steamgoods;

class steamParseSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steam:parseSite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        $html = file_get_contents('https://store.steampowered.com/search/?specials=1');
        $dom = HtmlDomParser::str_get_html($html);
        $perPage = 25;
        $total = 0;
        foreach($dom->find('.search_pagination_left') as $desc){
            preg_match('/showing (\d{1,5}) - (\d{2,4}) of (\d{4,6})/', trim($desc->plaintext), $o);
            $total = $o[3];
        }

        $pages = ceil($total/$perPage);

        Sales::where('status','>', -1)->update(['status' => -1]);
        
        for($i=1; $i<$pages+1; $i++){
            $ids = [];
            $html = file_get_contents("https://store.steampowered.com/search/?specials=1&page=$i");
            $dom = HtmlDomParser::str_get_html($html);
            echo "===== page $i =====\n\n";
            foreach($dom->find('.page_content > a') as $desc){
               if(!empty($desc->{'data-ds-appid'})){
                    if(stristr($desc->{'data-ds-appid'}, ',')){
                        $str = explode(',', $desc->{'data-ds-appid'});
                        
                        foreach($str as $s){
                            $ids[]=$s;
                        }

                    }else{
                        $ids[]=$desc->{'data-ds-appid'}; 
                    }
                }
                print_r($ids);
                foreach ($ids as $id) {
                    Sales::firstOrCreate([
                        'goods_id' => $id
                    ],[
                        'goods_id' => $id,
                        'status' => 0 
                    ]);
                }

                Sales::wherein('goods_id', $ids)->update(['status'=>0]);
            }
            echo "\n\n===== end page $i =====\n\n";
        }
     
        steamgoods::update(['sale' => 0]);   
    }
}


