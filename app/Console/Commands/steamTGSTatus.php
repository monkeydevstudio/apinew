<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamGoods;
use App\Models\telegramsSteam;

class steamTGSTatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:steam';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for($i=0; $i<11; $i++){
            $steam['success']=steamGoods::where('status', 1)->count();
            $steam['noData']=steamGoods::where('status', 2)->count();
            $steam['notUpdate']=steamGoods::where('status', 0)->count();

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/getUpdates');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_PROXY, '191.96.57.38:65233');
            curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
            curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
            curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
            $json = json_decode(curl_exec($curl),1);
            curl_close($curl);
            foreach ($json['result'] as $k => $j) {
                if($j['message']['text'] == '/steam'){
                    

                    /*$text = "Success: ".$steam['success'].", No data: ".$steam['noData'].", Not update:".$steam['notUpdate'];*/
                    $text = urlencode("Success: $steam[success]\r\nNo data: $steam[noData]\r\nNot update: $steam[notUpdate]");
                    $tid = telegramsSteam::firstOrCreate([
                        'message_id'=>$j['message']['message_id']
                    ],[
                        'message_id'=>$j['message']['message_id'],
                        'status'=>0 
                    ]);

                    if($tid->status !== 1 ){
                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, "https://api.telegram.org/bot650482100:AAECUdWBxptEYEiY5jjc7wuKlGJXT2s1Ugo/sendMessage?chat_id=448551297&text=$text");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                        curl_setopt($curl, CURLOPT_PROXY, '191.96.57.38:65233');
                        curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'sdemon072:J5k2UoT'); 
                        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); 
                        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
                        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1);
                        $upd = json_decode(curl_exec($curl),1);
                        curl_close($curl);
                        telegramsSteam::where('message_id', $j['message']['message_id'])->update(['status'=>1]);  
                    }    
                }
            } 
            sleep(5);   
        }
    }
}
