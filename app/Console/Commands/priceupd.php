<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Games;

class priceupd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:gamePrice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json=json_decode(file_get_contents('http://steammachine.ru/api/goodslist/?v=1&format=json'),1); 
        foreach ($json['response']['data']['goods'] as $k=>$v) {  
            
            $price=(isset($v['price']['rub'])?$v['price']['rub']:0);
            $available=$v['available'];
            $params=Games::where('sbid',$v['id_good'])->first();
            $update = [];
            if(isset($params->id)){
                $update=$params->toArray();
                $update['params']['price']=$price;
                $update['price']=$price;
                $update['params']['available']=$available;
                $update['available']=$available;
                $update['params']=json_encode($update['params']);
                Games::where('sbid',$v['id_good'])->update($update);
                echo "sbid=>$v[id_good], available=>$available, priceNew=>$price\n";
            }
            //$priceOld=(isset($params->params['price'])?$params->params['price']:$params->price);
           
            /*if(isset($params->id)){
                //echo "$params->id\n";
                $par=$params->params;
                //print_r($par);
                $par['price']=$price;
                $par['available']=$available;
                //$params=$par;
                $gid=Games::where('sbid',$v['id_good'])->update(['price'=>$price,'available'=>$available,'params'=>$par]);
                echo "sbid=>$v[id_good], stat=>$gid, priceNew=>$price\n";
            }*/
           
        }
    }
}
