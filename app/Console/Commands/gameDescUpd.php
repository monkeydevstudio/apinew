<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Games; 
class gameDescUpd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gameDescUpd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $rows = Games::limit(10000)->get();
        collect($rows)->each(function($g){
            $params=$g->toArray();
            if(empty(trim($params['params']['description']))){
                $platform = !empty($params['platform'])?$params['platform']:'Windows';
                $params['params']['description']="У нас вы можете купить лицензионный ключ игры $params[title], жанра $params[genre] по доступной цене, активация игры происходит в сервисе $params[activation] с установкой на $platform. Мгновенная доставка на электронный адрес, играйте легально!";
            }
            $params['params']['keyword']="купить $params[title], ключ $params[title]";
            $params['params']=json_encode($params['params']);
            Games::where('id',$params['id'])->update($params);
            echo "update $params[title] ($params[id])\n";
        }); 
    }
}
