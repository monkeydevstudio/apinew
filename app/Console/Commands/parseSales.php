<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\steamgoods;
use App\Models\Sales;

class parseSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            
        $ids = Sales::where('status', 0)->limit(12)->get();
        $countRow=Sales::where('status', 0)->count();
        $i = 1;
        foreach ($ids as $id) {
            $json = json_decode(file_get_contents('http://store.steampowered.com/api/appdetails?lg=ru&appids='.$id->goods_id),1);
            $json = $json[$id->goods_id];
            echo "ID: $id->goods_id\n";
            if(isset($json['data'])){
                steamgoods::updateOrCreate(
                [
                    'steam_id'=>$id->goods_id,   
                ],[
                    'steam_id' => $id->goods_id,
                    'title' => $json['data']['name'],
                    'price' => (array_key_exists('price_overview', $json['data']) ? $json['data']['price_overview']['final']/100 : 0),
                    'params' => $json['data'],
                    'slug' => str_slug($json['data']['name']),
                    'status' => 1,
                    'sale' =>  1,
                    'percent' => (array_key_exists('price_overview', $json['data']) ? $json['data']['price_overview']['discount_percent'] : 0),
                    'dlc' => (array_key_exists('type', $json['data']) && $json['data']['type'] === 'dlc' ? 1 : 0),
                    'parent_id' => (array_key_exists('type', $json['data']) && $json['data']['type'] === 'dlc' ? $json['data']['fullgame']['appid'] : 0),
                    'act' => 1
                ]);

                echo $json['data']['name']."($id->goods_id) succes update ($i/$countRow)\n"; 
                Sales::wherein('goods_id', $ids)->update(['status'=>1]);
            }
            $i++;
            sleep(5);
        }
    }
}
