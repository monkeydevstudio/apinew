<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\gameDescUpd',
        'App\Console\Commands\gkeysSiteMap',
        'App\Console\Commands\channelParser',
        'App\Console\Commands\channelStat',
        'App\Console\Commands\priceParser',
        'App\Console\Commands\priceupd',
        'App\Console\Commands\vkbot',
        'App\Console\Commands\parserNews',
        'App\Console\Commands\etnewspars',
        'App\Console\Commands\projectsUpdate',
        'App\Console\Commands\rewrite',
        'App\Console\Commands\etsitemap',
        'App\Console\Commands\steamParseApi',
        'App\Console\Commands\steamParseSite',
        'App\Console\Commands\telegramNews',
        'App\Console\Commands\telegtramTest',
        'App\Console\Commands\steamPriceParse',
        'App\Console\Commands\gameBackup',
        'App\Console\Commands\gameDump',
        'App\Console\Commands\parseSales',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
