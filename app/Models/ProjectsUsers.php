<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsUsers extends Model
{
    protected $fillable = ['user_id', 'projects_id'];
    public $timestamps = true;
}
