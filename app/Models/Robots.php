<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Robots extends Model
{
    protected $fillable = ['title','created_at','updated_at','params','category','link','rel_id','act'];
    protected $connection = 'news';	
    public $timestamps = true;
    public $translatable = ['params'];
    protected $casts = ['params' => 'json'];
}
