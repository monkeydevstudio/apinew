<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectsVideos extends Model
{	
	protected $fillable = ['videos_id', 'project_id'];
    protected $connection = 'channel';	
    public $timestamps = true;
}
