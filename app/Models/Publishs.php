<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publishs extends Model
{
    protected $fillable = ['robot_id','status'];
    protected $connection = 'news';	
    public $timestamps = false;
    /* public $translatable = ['params'];
    protected $casts = ['params' => 'json']; */
}
