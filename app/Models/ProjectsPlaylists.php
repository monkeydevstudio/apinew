<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectsPlaylists extends Model
{	
	protected $fillable = ['playlists_id', 'project_id'];
    protected $connection = 'channel';	
    public $timestamps = true;
}
