<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectsGames extends Model
{	
	protected $fillable = ['games_id', 'project_id'];
    protected $connection = 'gameshop';	
    public $timestamps = true;
}
