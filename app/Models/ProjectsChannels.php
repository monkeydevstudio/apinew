<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsChannels extends Model
{
	protected $fillable = ['channel_id', 'project_id'];
    protected $connection = 'channel';	
    public $timestamps = true;
}
