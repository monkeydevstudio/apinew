<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
	protected $fillable = ['title','sbid','availble','price','genre','activation','slug','params','act','status','platform'];
    protected $connection = 'gameshop';	
    public $timestamps = false;
   	//protected $hidden=['params'];
    //public $appends = ['at'];
    public $translatable = ['params'];
    protected $casts = ['params' => 'array'];

    public function projects_games(){
        return $this->hasOne('App\Models\ProjectsGames','games_id','id');
    }
    /*public function setAtAttribute(){
        return is_array($this->params) ? $this->params : (array) json_decode($this->params,1);
    }*/
}
