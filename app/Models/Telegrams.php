<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telegrams extends Model
{
    protected $fillable = ['robot_id','tid','status'];
    protected $connection = 'news';	
    public $timestamps = false;
    /* public $translatable = ['params'];
    protected $casts = ['params' => 'json']; */
}
