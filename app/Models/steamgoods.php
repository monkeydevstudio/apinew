<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class steamgoods extends Model
{
    protected $fillable = ['title','steam_id','price','slug','params','status','created_at','updated_at','deleted_at', 'sale', 'percent', 'dlc', 'parent_id','act'];
    protected $connection = 'gameshop';	
    public $timestamps = true;
   	//protected $hidden=['params'];
    //public $appends = ['at'];
    public $translatable = ['params'];
    protected $casts = ['params' => 'array'];

    public function projects_steam(){
        return $this->hasOne('App\Models\ProjectsSteam','games_id','id');
    }
}
