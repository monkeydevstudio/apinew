<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class videos extends Model
{
    protected $fillable = ['title','url','created_at','updated_at','slug','act','channel_id','playlist_id','params'];
    protected $connection = 'channel';

    public $timestamps = false;
   	//protected $hidden=['params'];
    //public $appends = ['at'];
    public $translatable = ['params'];
    protected $casts = ['params' => 'array'];

    public function projects_videos(){
        return $this->hasOne('App\Models\ProjectsVideos','video_id','id');
    }
    public function playlist(){
        return $this->hasOne('App\Models\Playlists','url','playlist_id');
    }

    function changeTextToLink($text){
	    $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" target=\"_blank\" rel=\"nofollow\">$3</a>", $text);
	    $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" target=\"_blank\" rel=\"nofollow\">$3</a>", $text);
	    return($text);
	}
    public function getParamsAttribute($value){
    	//print_r(json_decode($value,1));
    	//exit;
    	$params=json_decode($value,1);	
    	$params['snippet']['description']=$this->changeTextToLink(str_replace(array("\n","'",),array('<br/>',"&apos;"),$params['snippet']['description']));
    	return $params;
    }

    
}
