<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rel extends Model
{
    protected $fillable = ['title','slug','created_at','updated_at','params','act'];
    protected $connection = 'news';	
    public $timestamps = false;
}
