<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class telegramsSteam extends Model
{
    protected $fillable = ['message_id','status'];
    protected $connection = 'gameshop';	
    public $timestamps = false;
}
