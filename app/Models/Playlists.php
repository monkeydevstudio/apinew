<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlists extends Model
{
    protected $fillable = ['title','url','created_at','updated_at','slug','act','channel_id','playlist_id','params'];
    public $timestamps = false;
   	protected $connection = 'channel';	
   	//protected $hidden=['params'];
    //public $appends = ['at'];
    public $translatable = ['params'];
    protected $casts = ['params' => 'array'];

    public function projects_playlists(){
        return $this->hasOne('App\Models\ProjectsPlaylists','playlist_id','id');
    }

    public function videos(){
    	return $this->hasMany('App\Models\Videos','playlist_id','url')->orderBy('created_at','desc');
    }
}
