<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
    protected $fillable = ['url','act'];
    protected $connection = 'news';	
    public $timestamps = false;
    /* public $translatable = ['params'];
    protected $casts = ['params' => 'json']; */
}
