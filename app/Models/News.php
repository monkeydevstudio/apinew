<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title','slug','created_at','updated_at','params','act','pid'];
    protected $connection = 'news';	
    public $timestamps = true;
    //public $translatable = ['params'];
    protected $casts = ['params' => 'array'];

    public function projects_news(){
        return $this->hasOne('App\Models\ProjectsNews','news_id','id')->withDefault(['project_id' => 1]);
    }
}
