<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatisticsLog extends Model
{
    protected $fillable = ['video_id','params','type'];
    protected $connection = 'channel';

    public $timestamps = false;
}
