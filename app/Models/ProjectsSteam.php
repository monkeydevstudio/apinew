<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectsSteam extends Model
{
    protected $fillable = ['steamgoods_id', 'project_id'];
    protected $connection = 'gameshop';	
    public $timestamps = true;
}
