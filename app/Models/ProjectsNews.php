<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectsNews extends Model
{
	protected $fillable = ['news_id', 'project_id'];
    protected $connection = 'news';	
    public $timestamps = true;
}
