<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $fillable = ['goods_id','status'];
    protected $connection = 'gameshop';	
    public $timestamps = true;
    /* public $translatable = ['params'];
    protected $casts = ['params' => 'json']; */
}
