<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vkgoods extends Model
{
    protected $fillable = ['games_id','goods_id','groups_id'];
    protected $connection = 'gameshop';	
    public $timestamps = false;
}
